FROM node:20.10.0 as build-stage
WORKDIR /app
COPY package*.json ./
COPY babel.config.js ./
COPY . .
RUN npm install
RUN npm run build

FROM nginx:alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
